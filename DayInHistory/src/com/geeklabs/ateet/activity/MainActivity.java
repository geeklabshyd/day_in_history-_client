package com.geeklabs.ateet.activity;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.geeklabs.ateet.R;
import com.geeklabs.ateet.activity.adapter.CategoryAdapter;
import com.geeklabs.ateet.activity.dto.InputDto;
import com.geeklabs.ateet.activity.pref.PerPreferences;
import com.geeklabs.ateet.activity.util.LocationServices;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;

public class MainActivity extends Activity implements ConnectionCallbacks,
		OnConnectionFailedListener {

	protected Context context;
	private PerPreferences preferences;

	// A request to connect to Location Services
	private LocationRequest mLocationRequest;

	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preferences = new PerPreferences(getApplicationContext());

		// Create Location req and Location client
		setUpLocationClientIfNeeded();
		// Connect the client.
		if (!mLocationClient.isConnected()) {
			mLocationClient.connect();
		}

		/*
		 * Location location = mLocationClient.getLastLocation(); if (location
		 * != null && preferences.isLastLocationChanged(location)) {
		 * updateUserLocation(location); }
		 */

		setContentView(R.layout.activity_main);
		final GridView gridView = (GridView) findViewById(R.id.grid_view);
		final CategoryAdapter categoryAdapter = new CategoryAdapter(this);
		gridView.setAdapter(categoryAdapter);

		/*
		 * SharedPreferences mySettings; mySettings = getSharedPreferences("",
		 * Context.MODE_PRIVATE); int gridSize = 40 *
		 * Integer.parseInt(mySettings.getString("gridSize", "3"));
		 * gridView.setColumnWidth(gridSize);
		 */

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Integer gridviewClickedItem = (Integer) gridView.getAdapter()
						.getItem(position);
				// create input data
				InputDto dto = new InputDto();
				dto.setDate(new Date());
				dto.setCategory(categoryAdapter
						.getItemString(gridviewClickedItem));
				dto.setCity(preferences.getLocation());

				Intent intent = new Intent(MainActivity.this,
						DayEventActivity.class);
				intent.putExtra("com.geeklabs.dayinhistory.inputCategory", dto);
				MainActivity.this.startActivity(intent);
			}
		});

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// If the client is connected
		if (mLocationClient != null && mLocationClient.isConnected()) {
			/*
			 * Remove location updates for a listener. The current Activity is
			 * the listener, so the argument is "this".
			 */
			mLocationClient.disconnect();
		}
	}

	private void setUpLocationClientIfNeeded() {
		createLocReq();
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(getApplicationContext(), this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}
	}

	private void createLocReq() {
		if (mLocationRequest == null) {
			// Create a new global location parameters object
			mLocationRequest = LocationRequest.create();
			// Set the update interval
			mLocationRequest
					.setInterval(LocationServices.UPDATE_INTERVAL_IN_MILLISECONDS);
			// Use high accuracy
			mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
			// Set the interval ceiling to one minute
			mLocationRequest
					.setFastestInterval(LocationServices.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		}
	}

	public void updateUserLocation(Location location) {
		Geocoder geocoder;
		List<Address> addresses = null;
		geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
		try {
			addresses = geocoder.getFromLocation(location.getLatitude(),
					location.getLongitude(), 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (addresses != null && addresses.size() > 0) {
			String city = addresses.get(0).getLocality();
			String state = addresses.get(0).getAdminArea();
			String country = addresses.get(0).getCountryName();

			preferences.setLocation(city + ", " + state + ", " + country);
			preferences.setLastLocationLat(location.getLatitude());
			preferences.setLastLocationLng(location.getLongitude());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_day_event:
			Intent intent = new Intent(this, NewDayEventActivity.class);
			startActivity(intent);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle arg0) {
		Location location = mLocationClient.getLastLocation();
		if (location != null && preferences.isLastLocationChanged(location)) {
			updateUserLocation(location);
		}
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

}
