package com.geeklabs.ateet.activity.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class NetworkService {

	private NetworkService() {
	}

	public static boolean isNetWorkAvailable(final Context contextActivity) {
		ConnectivityManager conMgr = (ConnectivityManager) contextActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnected();
	}

	public static boolean isWifiConnected(final Context context) {
		ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		return ((networkInfo != null) && networkInfo.isConnected());
	}
	
	public static boolean isMobileDateConnected(final Context context) {
		ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		return ((networkInfo != null) && networkInfo.isConnected());
	}
	
	public static boolean isWifiOrDataEnabled(final Context context) {
		
		if (isWifiConnected(context)) {
			 return true;
		}
		
		if (isMobileDateConnected(context)) {
			return true;
		}
		
		 return false;
	}
}
