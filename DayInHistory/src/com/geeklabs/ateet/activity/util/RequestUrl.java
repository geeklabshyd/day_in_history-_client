package com.geeklabs.ateet.activity.util;

public final class RequestUrl {

	private RequestUrl(){
	}
	//Test
	public static final String GET_DAY_EVENTS_BY_CATEGORY = "http://192.168.1.107:8899/admin/dayevent/get";
	public static final String NEW_DAY_EVENT ="http://192.168.1.107:8899/admin/dayevent/addfromuser";
	public static final String DAY_EVENT_LIKE_COUNT ="http://192.168.1.107:8899/admin/dayevent/likecount/{dayEventId}";
	
	//Deployement
	/*public static final String GET_DAY_EVENTS_BY_CATEGORY = "http://m-ateet.appspot.com/admin/dayevent/get";
	public static final String NEW_DAY_EVENT ="http://m-ateet.appspot.com/admin/dayevent/addfromuser";
	public static final String DAY_EVENT_LIKE_COUNT ="http://m-ateet.appspot.com/admin/dayevent/likecount/{dayEventId}";*/
}
