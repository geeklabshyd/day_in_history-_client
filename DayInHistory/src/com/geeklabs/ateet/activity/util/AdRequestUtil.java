package com.geeklabs.ateet.activity.util;

import com.google.android.gms.ads.AdRequest;

public final class AdRequestUtil {
	private AdRequestUtil() {
	}

	public static AdRequest getAdRequest() {
		// Request for Ads
		AdRequest adRequest = new AdRequest.Builder()
		// Add a test device to show Test Ads
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
		// Load ads into Banner Ads

		return adRequest;
	}

}
