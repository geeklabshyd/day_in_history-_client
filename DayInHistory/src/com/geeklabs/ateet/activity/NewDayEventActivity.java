package com.geeklabs.ateet.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.R.integer;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.geeklabs.ateet.R;
import com.geeklabs.ateet.activity.adapter.PlacesAutoCompleteAdapter;
import com.geeklabs.ateet.activity.commmunication.task.post.NewDayEventTask;
import com.geeklabs.ateet.activity.dto.NewDayEventDto;

/**
 * Auto suggestion resource :
 * https://developers.google.com/places/training/autocomplete-android
 */
public class NewDayEventActivity extends Activity implements
		OnItemClickListener {

	private Spinner category;
	private AutoCompleteTextView city;
	private TextView dateView;
	private EditText content;
	private Spinner weight;
	private EditText username;
	private EditText email;
	private Button add_day_event;

	private DatePicker datePicker;
	private Calendar calendar;
	private int year, month, day;
	private Activity activity;

	private String category_selected;
	private String city_selected;
	private Date date;
	protected int weight_selected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_day_event);

		category = (Spinner) findViewById(R.id.category);
		city = (AutoCompleteTextView) findViewById(R.id.city);
		dateView = (TextView) findViewById(R.id.date);
		content = (EditText) findViewById(R.id.content);
		weight = (Spinner) findViewById(R.id.weight);
		username = (EditText) findViewById(R.id.username);
		email = (EditText) findViewById(R.id.email);
		add_day_event = (Button) findViewById(R.id.add_day_event_button);

		// Category View
		ArrayAdapter<CharSequence> category_adapter = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item);
		category_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		category.setAdapter(category_adapter);
		
		// City View
		city.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.list_item));
		city.setThreshold(1);
		
		// Date View
		calendar = Calendar.getInstance();
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH)+1;
		day = calendar.get(Calendar.DATE);
		showDate(year, month, day);
		
		// Weght View
				ArrayAdapter<CharSequence> weight_adapter = ArrayAdapter.createFromResource(this, R.array.weight, android.R.layout.simple_spinner_item);
				weight_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				weight.setAdapter(weight_adapter);

		// Add Day event
		add_day_event.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View view) {
				NewDayEventDto newDayEventDto = new NewDayEventDto();
				category_selected = ((Spinner) findViewById(R.id.category))
						.getSelectedItem().toString();
				city_selected = city.getText().toString();
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
				format.setCalendar(calendar);
				try {
					date = format.parse(dateView.getText().toString());
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
				weight_selected = Integer.parseInt((String)((Spinner) findViewById(R.id.weight))
						.getSelectedItem());
				
				newDayEventDto.setId(null);
				newDayEventDto.setCategoryName(category_selected);
				newDayEventDto.setCity(city_selected);
				newDayEventDto.setEventDate(date);
				newDayEventDto.setContent(content.getText().toString());
				newDayEventDto.setWeight(weight_selected);
				newDayEventDto.setUserName((username.getText().toString()));
				newDayEventDto.setEmail(email.getText().toString());
				newDayEventDto.setCreatedDate(new Date());

				// call asynctask and add day event AddDayEventTask
				// addDayEventTask =
				NewDayEventTask newDayEventTask = new NewDayEventTask(null,
						NewDayEventActivity.this, newDayEventDto);
				newDayEventTask.execute();
				finish();

			}

		});
	}

	@SuppressWarnings("deprecation")
	public void setDate(View view) {
		showDialog(999);
		Toast.makeText(getApplicationContext(), "Change date",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 999) {
			return new DatePickerDialog(this, myDateListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub
			// arg1 = year
			// arg2 = month
			// arg3 = day
			showDate(arg1, arg2 + 1, arg3);
		}
	};

	private void showDate(int year, int month, int day) {
		dateView.setText(new StringBuilder().append(year).append("-")
				.append(month).append("-").append(day));
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		String str = (String) adapterView.getItemAtPosition(position);
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}
}
