package com.geeklabs.ateet.activity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.geeklabs.ateet.R;
import com.geeklabs.ateet.activity.adapter.DayEventsAdapter;
import com.geeklabs.ateet.activity.adapter.PlacesAutoCompleteAdapter;
import com.geeklabs.ateet.activity.commmunication.task.get.GetDayEventsByCategoryTask;
import com.geeklabs.ateet.activity.domain.DayEvent;
import com.geeklabs.ateet.activity.dto.InputDto;
import com.geeklabs.ateet.activity.pref.PerPreferences;
import com.geeklabs.ateet.activity.util.AdRequestUtil;
import com.google.android.gms.ads.AdView;

public class DayEventActivity extends ListActivity {

	private Intent intent;
	private TextView changeDate;
	private Calendar calendar;
	protected Context activity;
	private DayEventsAdapter dayEventsAdapter;
	private Date modifiedDate;
	private AdView adView;
	private TextView location;
	private AutoCompleteTextView cityAutoSggestion;
	private PerPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_day_event);

		preferences = new PerPreferences(getApplicationContext());

		cityAutoSggestion = (AutoCompleteTextView) findViewById(R.id.cityAutoSuggest);
		cityAutoSggestion.setVisibility(View.GONE);

		location = (TextView) findViewById(R.id.location);
		changeDate = (TextView) findViewById(R.id.today_date);

		// Set User Location

		String userLoc = preferences.getLocation();
		String[] splitLoc = userLoc.split(", ");

		if (splitLoc != null && splitLoc.length > 0) {
			location.setText(splitLoc[0]);
		} else {
			location.setText(userLoc);// UnKnown
		}

		// Change location
		changeLocationEvent();

		// Set today's date
		calendar = Calendar.getInstance();

		String myFormat = "MMM dd"; // In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
		changeDate.setText(sdf.format(calendar.getTime()));

		changeDate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new DatePickerDialog(DayEventActivity.this,
						onDateChangeListener(), calendar.get(Calendar.YEAR),
						calendar.get(Calendar.MONTH), calendar
								.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

		// get day events list
		intent = getIntent();
		List<DayEvent> dayEvents = new ArrayList<DayEvent>();
		dayEventsAdapter = new DayEventsAdapter(DayEventActivity.this,
				dayEvents);

		setListAdapter(dayEventsAdapter);
		setupActionBar();

		InputDto inputCategory = (InputDto) intent
				.getSerializableExtra("com.geeklabs.dayinhistory.inputCategory");

		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage("Loading...");
		progress.setCancelable(true);
		progress.show();

		GetDayEventsByCategoryTask getDayEventsByCategoryTask = new GetDayEventsByCategoryTask(
				progress, DayEventActivity.this, inputCategory, 0) {
			@Override
			protected void notifyAdapter(List<DayEvent> dayEvents) {
				dayEventsAdapter.setDayEvents(dayEvents);
				dayEventsAdapter.notifyDataSetChanged();
			}
		};

		getDayEventsByCategoryTask.execute();

		// Adds code
		// http://www.androidbegin.com/tutorial/integrating-new-google-admob-banner-interstitial-ads/
		adView = (AdView) this.findViewById(R.id.adView);

		adView.loadAd(AdRequestUtil.getAdRequest());
	}

	private DatePickerDialog.OnDateSetListener onDateChangeListener() {
		return new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				calendar.set(Calendar.YEAR, year);
				calendar.set(Calendar.MONTH, monthOfYear);
				calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				Date date = calendar.getTime();

				String modifiedDateisString = (String.valueOf(monthOfYear + 1)
						+ " " + String.valueOf(dayOfMonth) + " " + String
						.valueOf(year));
				updateLabel(modifiedDateisString);

				intent = getIntent();
				InputDto inputCategory = (InputDto) intent
						.getSerializableExtra("com.geeklabs.dayinhistory.inputCategory");
				inputCategory.setDate(date);

				final ProgressDialog progress = new ProgressDialog(
						DayEventActivity.this);
				progress.setMessage("Loading...");
				progress.setCancelable(true);
				progress.show();

				GetDayEventsByCategoryTask getDayEventsByCategoryTask = new GetDayEventsByCategoryTask(
						progress, DayEventActivity.this, inputCategory, 0) {
					@Override
					protected void notifyAdapter(List<DayEvent> dayEvents) {
						dayEventsAdapter.clear();
						dayEventsAdapter.setDayEvents(dayEvents);
						dayEventsAdapter.notifyDataSetChanged();
					}
				};

				getDayEventsByCategoryTask.execute();
			}
		};
	}

	private void changeLocationEvent() {
		location.setOnClickListener(new TextView.OnClickListener() {
			@Override
			public void onClick(View view) {

				location.setVisibility(View.GONE);
				changeDate.setVisibility(View.GONE);
				cityAutoSggestion.setVisibility(View.VISIBLE);
				cityAutoSggestion.setText("");

				cityAutoSggestion.setAdapter(new PlacesAutoCompleteAdapter(
						DayEventActivity.this, R.layout.list_item));
				cityAutoSggestion.setThreshold(1);

				cityAutoSggestion
						.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								location.setVisibility(View.VISIBLE);
								changeDate.setVisibility(View.VISIBLE);
								cityAutoSggestion.setVisibility(View.GONE);
								cityAutoSggestion.clearFocus();
								InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(
										cityAutoSggestion.getWindowToken(), 0);

								Editable city = cityAutoSggestion.getText();
								if (city != null && city.toString() != null) {
									InputDto inputCategory = (InputDto) intent
											.getSerializableExtra("com.geeklabs.dayinhistory.inputCategory");
									inputCategory.setCity(city.toString());
									String[] splitLoc = city.toString().split(
											", ");

									if (splitLoc != null && splitLoc.length > 0) {
										location.setText(splitLoc[0]);
									}
									// show
									// ONLY
									// city/state/country
									// name

									final ProgressDialog progress = new ProgressDialog(
											DayEventActivity.this);
									progress.setMessage("Loading...");
									progress.setCancelable(true);
									progress.show();

									GetDayEventsByCategoryTask getDayEventsByCategoryTask = new GetDayEventsByCategoryTask(
											progress, DayEventActivity.this,
											inputCategory, 0) {
										@Override
										protected void notifyAdapter(
												List<DayEvent> dayEvents) {
											dayEventsAdapter.clear();
											dayEventsAdapter
													.setDayEvents(dayEvents);
											dayEventsAdapter
													.notifyDataSetChanged();
										}
									};

									getDayEventsByCategoryTask.execute();
								}
								;
							};
						});

			}

			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long id) {
				String str = (String) adapterView.getItemAtPosition(position);
			}

		});
	}

	protected void updateLabel(String modifieddate) {
		String myFormat = "MMM dd"; // In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
		changeDate.setText(sdf.format(calendar.getTime()));

		DateFormat format = new SimpleDateFormat("M dd yyyy");
		try {
			modifiedDate = format.parse(modifieddate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_day_event:
			Intent intent = new Intent(this, NewDayEventActivity.class);
			startActivity(intent);
			// Toast.makeText(getApplicationContext(),"Item 1 Selected",Toast.LENGTH_LONG).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
