package com.geeklabs.ateet.activity.commmunication.task.get;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.ateet.activity.DayEventActivity;
import com.geeklabs.ateet.activity.communication.AbstractHttpPostTask;
import com.geeklabs.ateet.activity.domain.DayEvent;
import com.geeklabs.ateet.activity.dto.InputDto;
import com.geeklabs.ateet.activity.util.RequestUrl;

public abstract class GetDayEventsByCategoryTask extends AbstractHttpPostTask {

	private InputDto inputDto;
	private Activity activity;
	private int currentPage;

	public GetDayEventsByCategoryTask(ProgressDialog progressDialog,
			Activity context, InputDto dto, int currentPage) {
		super(progressDialog, context);
		this.inputDto = dto;
		this.activity = context;
		this.currentPage = currentPage;
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(inputDto);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.GET_DAY_EVENTS_BY_CATEGORY;
	}

	@Override
	protected void showMessageOnUI(String msg) {
		Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			List<DayEvent> dayEvents = new ArrayList<DayEvent>();
			ObjectMapper mapper = new ObjectMapper();
			try {
				dayEvents = mapper.readValue(jsonResponse,new TypeReference<List<DayEvent>>() {});
				
				// Notify events list view
				notifyAdapter(dayEvents);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {			
			showMessageOnUI("Results not found...");
		}
	}

	protected abstract void notifyAdapter(List<DayEvent> dayEvents);

	@Override
	protected void addExtraParams(List<NameValuePair> params) {
		NameValuePair basicNameValuePair = new BasicNameValuePair(
				"currentPage", String.valueOf(currentPage));
		params.add(basicNameValuePair);
	}
}
