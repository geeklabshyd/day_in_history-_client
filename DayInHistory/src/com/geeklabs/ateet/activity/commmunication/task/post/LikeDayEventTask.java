package com.geeklabs.ateet.activity.commmunication.task.post;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.ateet.activity.DayEventActivity;
import com.geeklabs.ateet.activity.NewDayEventActivity;
import com.geeklabs.ateet.activity.communication.AbstractHttpPostTask;
import com.geeklabs.ateet.activity.domain.DayEvent;
import com.geeklabs.ateet.activity.dto.NewDayEventDto;
import com.geeklabs.ateet.activity.util.DataWrapper;
import com.geeklabs.ateet.activity.util.RequestUrl;

public class LikeDayEventTask extends AbstractHttpPostTask {

	private Activity activity;
	private long eventId;

	public LikeDayEventTask(ProgressDialog progressDialog, Activity context, 
			long eventId) {
		super(progressDialog, context);
		this.activity = context;
		this.eventId = eventId;
	}


	@Override
	protected String getRequestUrl() {
		return RequestUrl.DAY_EVENT_LIKE_COUNT.replace("{dayEventId}",String.valueOf(eventId)); 

	}

	@Override
	protected void showMessageOnUI(String string) {
		
		activity.runOnUiThread(new Runnable() {
			private String message;

			public void run() {
				Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
				Log.i("You liked this event", message);
			}
		});
		

	}

	@Override
	protected String getRequestJSON() {
		return null;
	}


	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		// TODO Auto-generated method stub
		
	}

}
