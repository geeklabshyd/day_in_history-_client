package com.geeklabs.ateet.activity.commmunication.task.post;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.ateet.activity.DayEventActivity;
import com.geeklabs.ateet.activity.NewDayEventActivity;
import com.geeklabs.ateet.activity.communication.AbstractHttpPostTask;
import com.geeklabs.ateet.activity.domain.DayEvent;
import com.geeklabs.ateet.activity.dto.NewDayEventDto;
import com.geeklabs.ateet.activity.util.DataWrapper;
import com.geeklabs.ateet.activity.util.RequestUrl;

public class NewDayEventTask extends AbstractHttpPostTask {

	private Activity activity;
	private NewDayEventDto newDayEventDto;

	public NewDayEventTask(ProgressDialog progressDialog, Activity context,
			NewDayEventDto newDayEventDto) {
		super(progressDialog, context);
		this.newDayEventDto = newDayEventDto;
		this.activity = context;
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(newDayEventDto);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.NEW_DAY_EVENT;
	}

	@Override
	protected void showMessageOnUI(String string) {

	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {

		if (!jsonResponse.isEmpty()) {
			Toast.makeText(activity, "Event Added Successfully", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(activity, "Failed to add an Event", Toast.LENGTH_SHORT).show();
		}
	}

}
