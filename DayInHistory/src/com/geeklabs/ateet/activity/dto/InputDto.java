package com.geeklabs.ateet.activity.dto;

import java.io.Serializable;
import java.util.Date;

public class InputDto implements Serializable {

	private String category;
	private Date date;
	private String city;
	
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
	
}
