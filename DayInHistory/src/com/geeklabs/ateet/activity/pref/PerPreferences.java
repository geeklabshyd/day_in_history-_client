package com.geeklabs.ateet.activity.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;

public class PerPreferences {

	private static String LOCATION = "location";
	private static String LAST_LOCATION_LAT = "lastKnownLat";
	private static String LAST_LOCATION_LNG = "lastKnownLng";
	
	private SharedPreferences preferences;
	
	public PerPreferences(Context context){
		preferences = context.getSharedPreferences("dayHistory_personalized_preferences", Context.MODE_PRIVATE);
	}
	
	public  void setLocation(String location) {
		Editor editor = preferences.edit();
		editor.putString(LOCATION, location);
		editor.commit();
	}
	
	public String getLocation() {
		return preferences.getString(LOCATION, "UnKnown");
	}
	
	public void setLastLocationLat(double lat) {
		Editor editor = preferences.edit();
		editor.putFloat(LAST_LOCATION_LAT, (float)lat);
		editor.commit();
	}
	
	public double getLastLocationLat() {
		return preferences.getFloat(LAST_LOCATION_LAT, 0.0f);
	}
	
	public void setLastLocationLng(double lat) {
		Editor editor = preferences.edit();
		editor.putFloat(LAST_LOCATION_LNG, (float)lat);
		editor.commit();
	}
	
	public double getLastLocationLng() {
		return preferences.getFloat(LAST_LOCATION_LNG, 0.0f);
	}
	
	public boolean isLastLocationChanged(Location currentlocation) {
		if (getLastLocationLat() != currentlocation.getLatitude() || getLastLocationLng() != currentlocation.getLongitude()) {
			return true;
		}
		
		return false;
	}
	
	public void clearPreferences() {
		Editor editor = preferences.edit();
		
		editor.remove(LOCATION);
		
		editor.commit();
	}
	
}
