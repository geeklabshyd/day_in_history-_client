package com.geeklabs.ateet.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.text.TextUtils.TruncateAt;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.geeklabs.ateet.R;
import com.geeklabs.ateet.activity.commmunication.task.post.LikeDayEventTask;
import com.geeklabs.ateet.activity.domain.DayEvent;

public class DayEventsAdapter extends BaseAdapter {

	private List<DayEvent> dayEvents = new ArrayList<DayEvent>();
	private Activity activity;
	private DayEvent dayEvent;
	private LayoutInflater inflater;

	public List<DayEvent> getDayEvents() {
		return dayEvents;
	}

	public void setDayEvents(List<DayEvent> dayEvents) {
		this.dayEvents = dayEvents;
		notifyDataSetChanged();
	}

	public void clear() {
		dayEvents.clear();
	}

	public DayEventsAdapter(Activity activity, List<DayEvent> dayEvents) {
		this.dayEvents.clear();
		this.dayEvents = dayEvents;
		this.activity = activity;
		this.inflater = LayoutInflater.from(activity);
	}

	@Override
	public int getCount() {
		return dayEvents.size();
	}

	@Override
	public Object getItem(int position) {
		return dayEvents.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		dayEvent = dayEvents.get(position);
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.activity_dayevent__item,
					null);

			holder.yearTextView = (TextView) convertView
					.findViewById(R.id.year);
			holder.contentTextView = (TextView) convertView
					.findViewById(R.id.content);
			holder.likeImageView = (ImageView) convertView
					.findViewById(R.id.like);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.yearTextView.setText("" + dayEvent.getYear());
		holder.contentTextView.setText(dayEvent.getContent());

		holder.contentTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// expand the item
				TruncateAt ellipsize = holder.contentTextView.getEllipsize();
				if (ellipsize != null) {
					holder.contentTextView.setEllipsize(null);
					holder.contentTextView.setMaxLines(holder.contentTextView
							.getText().toString().length());
				} else {
					holder.contentTextView.setEllipsize(TruncateAt.END);
					holder.contentTextView.setMaxLines(3);
				}
			}
		});

		holder.likeImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dayEvent = dayEvents.get(position);
				long eventID = dayEvent.getId();
				LikeDayEventTask likeDayEventTask = new LikeDayEventTask(null,
						activity, eventID);
				likeDayEventTask.execute();
				Toast.makeText(activity, "You liked this event",
						Toast.LENGTH_SHORT).show();
			}
		});

		/*
		 * holder.likeButton.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { dayEvent =
		 * dayEvents.get(position); Long eventId = dayEvent.getId();
		 * LikeDayEventTask likeDayEventTask = new LikeDayEventTask(null,
		 * activity, eventId){
		 * 
		 * }; likeDayEventTask.execute(); } });
		 */

		return convertView;
	}

	private static class ViewHolder {
		public ImageView expandButton;
		TextView yearTextView, contentTextView;
		ImageView likeImageView;

	}
}
