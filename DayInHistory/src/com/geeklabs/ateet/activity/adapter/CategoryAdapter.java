package com.geeklabs.ateet.activity.adapter;

import java.util.HashMap;

import com.geeklabs.ateet.R;
import com.geeklabs.ateet.R.dimen;
import com.geeklabs.ateet.R.drawable;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class CategoryAdapter extends BaseAdapter {
	private Context mContext;

	// Keep all Images in array
	private Integer[] mThumbIds = { R.drawable.sports,R.drawable.films, R.drawable.technology,
									R.drawable.science, R.drawable.politics, R.drawable.war, 
									R.drawable.general, R.drawable.birthdays, R.drawable.deaths
	};
	
	private final HashMap<Integer, String> itemValues = new HashMap<Integer, String>();
	
	// Constructor
	public CategoryAdapter(Context c) {
		mContext = c;
		
		itemValues.put(R.drawable.sports, "Sports");
		itemValues.put(R.drawable.films, "Films");
		itemValues.put(R.drawable.technology, "Technology");
		itemValues.put(R.drawable.science, "Science");
		itemValues.put(R.drawable.politics, "Politics");
		itemValues.put(R.drawable.war, "Wars");
		itemValues.put(R.drawable.general, "General");
		itemValues.put(R.drawable.birthdays, "BirthDeathDays");
		itemValues.put(R.drawable.deaths, "Deaths");
	}
	
	public String getItemString(int thumbId) {
		return itemValues.get(thumbId);
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return mThumbIds[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView = new ImageView(mContext);
		imageView.setImageResource(mThumbIds[position]);
		imageView.setScaleType(ImageView.ScaleType.FIT_END);
		/*DisplayMetrics metrics= mContext.getResources().getDisplayMetrics();
		int deviceWidth = metrics.widthPixels;

        int deviceHeight = metrics.heightPixels;

        float widthInPercentage =  ( (float) 80 / 320 )  * 100; // 280 is the width of my LinearLayout and 320 is device screen width as i know my current device resolution are 320 x 480 so i'm calculating how much space (in percentage my layout is covering so that it should cover same area (in percentage) on any other device having different resolution

        float heightInPercentage =  ( (float) 80 / 480 ) * 100; // same procedure 300 is the height of the LinearLayout and i'm converting it into percentage

        int mLayoutWidth = (int) ( (widthInPercentage * deviceWidth) / 100 );

        int mLayoutHeight = (int) ( (heightInPercentage * deviceHeight) / 100 );
        
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(mLayoutWidth, mLayoutHeight);

        //LayoutParams layoutParams = new LayoutParams(mLayoutWidth, mLayoutHeight);

        imageView.setLayoutParams(params);*/
		imageView.setLayoutParams(new GridView.LayoutParams((int) mContext.getResources().getDimension(R.dimen.cell_width),
															(int) mContext.getResources().getDimension(R.dimen.cell_height)));

		return imageView;
	}
	/*final ViewTreeObserver mLayoutObserver = mLayout.getViewTreeObserver();

    mLayoutObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() 
    {

        @Override
        public void onGlobalLayout() 
        {
            DisplayMetrics metrics = getResources().getDisplayMetrics();

            int deviceWidth = metrics.widthPixels;

            int deviceHeight = metrics.heightPixels;

            float widthInPercentage =  ( (float) 280 / 320 )  * 100; // 280 is the width of my LinearLayout and 320 is device screen width as i know my current device resolution are 320 x 480 so i'm calculating how much space (in percentage my layout is covering so that it should cover same area (in percentage) on any other device having different resolution

            float heightInPercentage =  ( (float) 300 / 480 ) * 100; // same procedure 300 is the height of the LinearLayout and i'm converting it into percentage

            int mLayoutWidth = (int) ( (widthInPercentage * deviceWidth) / 100 );

            int mLayoutHeight = (int) ( (heightInPercentage * deviceHeight) / 100 );

            LayoutParams layoutParams = new LayoutParams(mLayoutWidth, mLayoutHeight);

            mLayout.setLayoutParams(layoutParams);
        }
    });*/

}